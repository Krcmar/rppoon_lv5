﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZAD5
{
    class Program
    {
        static void Main(string[] args)
        {
            LightTheme lightTheme = new LightTheme();
            BlueTheme blueTheme = new BlueTheme();
            ReminderNote note = new ReminderNote("Kaizoku oni ore wa naru!", lightTheme);
            note.Show();
            note = new ReminderNote("Bring some meat!", blueTheme);
            note.Show();
            Console.ReadKey();
        }
    }
}
