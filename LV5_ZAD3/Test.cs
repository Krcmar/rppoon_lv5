﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZAD3
{
    class Test
    {
        static void Main(string[] args)
        {
            User user1, user2;
            user1 = User.GenerateUser("User one");
            user2 = User.GenerateUser("User two");

            DataConsolePrinter dataConsolePrinter = new DataConsolePrinter();

            Dataset dataset = new Dataset("Test.csv");
            Console.WriteLine("Output using object from Dataset:");
            dataConsolePrinter.ConsolePrinter(dataset);

            Console.WriteLine("\nOutput using object from Dataset with Virtual Proxy:");
            VirtualProxyDataset virtualProxyDataset = new VirtualProxyDataset("Test.csv");
            dataConsolePrinter.ConsolePrinter(virtualProxyDataset);

            Console.WriteLine("\nOutput using object from Dataset with Protection Proxy, user ID=1:");
            ProtectionProxyDataset protectionProxyDataset = new ProtectionProxyDataset(user1);
            dataConsolePrinter.ConsolePrinter(protectionProxyDataset);

            Console.WriteLine("\nOutput using object from Dataset with Protection Proxy, user ID=2:");
            protectionProxyDataset = new ProtectionProxyDataset(user2);
            dataConsolePrinter.ConsolePrinter(protectionProxyDataset);
        }
    }
}
