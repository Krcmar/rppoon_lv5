﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ZAD3
{
    class DataConsolePrinter
    {
        public DataConsolePrinter() { }

        public void ConsolePrinter(IDataset data)
        {
            IReadOnlyCollection<List<string>> temporaryData = data.GetData();
            if (temporaryData != null)
            {
                foreach (List<string> list in temporaryData)
                {
                    list.ForEach(i => Console.Write(i + " "));
                    Console.WriteLine(" ");
                }
            }
            else
                Console.WriteLine("ERROR. GetData() returned NULL.");
        }
    }
}

