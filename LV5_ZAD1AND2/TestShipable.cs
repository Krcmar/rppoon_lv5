﻿using System;

namespace LV5
{
    class TestShippable
    {
        static void Main(string[] args)
        {
            Product dvd = new Product("Godfather", 19.99, 0.2);
            Product tool = new Product("Hammer", 34.98, 0.45);

            Box ListOfProducts = new Box("Products");
            ListOfProducts.Add(dvd);
            ListOfProducts.Add(tool);

            Console.WriteLine("Product price: " + ListOfProducts.Price);
            Console.WriteLine("Product weight: " + ListOfProducts.Weight);
            Console.WriteLine("Product description: " + ListOfProducts.Description());

            ShippingService ShippingList = new ShippingService(3);
            Console.WriteLine("Shipping price: " + ShippingList.GetPrice(ListOfProducts));
            Console.WriteLine("Shipping price for the hammer: " + ShippingList.GetPrice(tool));

        }
    }
}
