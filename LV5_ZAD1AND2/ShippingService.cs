﻿using System;
using System.Collections.Generic;
using System.Text;

class ShippingService
    {
    private double pricePerMass;
    public ShippingService(double pricePerMass)
    {
        this.pricePerMass = pricePerMass;
    }
    public double GetPrice(IShipable products)
    {
        return products.Weight * this.pricePerMass;
    }
}
