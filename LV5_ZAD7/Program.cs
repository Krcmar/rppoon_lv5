﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZAD5
{
    class Program
    {
        static void Main(string[] args)
        {
            LightTheme lightTheme = new LightTheme();
            BlueTheme blueTheme = new BlueTheme();
            
            GroupNote crazyGroupNote = new GroupNote("We've arrived to Wano!", "Nami", lightTheme);
            crazyGroupNote.Add("Luffy");
            crazyGroupNote.Add("Zoro");
            crazyGroupNote.Show();

            GroupNote lesscrazyGroupNote = new GroupNote("Shishishi! Let's kick Kaido's ass!", "Luffy", blueTheme);
            lesscrazyGroupNote.Add("Sanji");
            lesscrazyGroupNote.Show();

            crazyGroupNote.Remove("Zoro");
            crazyGroupNote.Show();

            lesscrazyGroupNote.Add("Chopper");
            lesscrazyGroupNote.Show();


            Notebook DeathNote = new Notebook();
            DeathNote.AddNote(crazyGroupNote);
            DeathNote.AddNote(lesscrazyGroupNote);
            DeathNote.Display();

            DeathNote = new Notebook(blueTheme);
            DeathNote.AddNote(crazyGroupNote);
            DeathNote.AddNote(lesscrazyGroupNote);
            DeathNote.Display(); ;
        }
    }
}
