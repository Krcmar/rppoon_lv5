﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ZAD3
{
    class ConsoleLogger
    {
        static ConsoleLogger instance;

        private ConsoleLogger()
        {
        }
        public static ConsoleLogger GetInstance()
        {
            if (instance == null)
                instance = new ConsoleLogger();
            return instance;
        }
        public void Log()
        {
            Console.WriteLine("GetData() has been called at this time: " + DateTime.Now);
        }
    }
}
