﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace ZAD3
{
    class VirtualProxyLoggingDataset : IDataset
    {
        private string filePath;
        private Dataset dataset;

        public VirtualProxyLoggingDataset(string filePath)
        {
            this.filePath = filePath;
        }
        public ReadOnlyCollection<List<string>> GetData()
        {
            if (dataset == null)
            {
                dataset = new Dataset(filePath);
            }
            ConsoleLogger.GetInstance().Log();
            return dataset.GetData();
        }
    }
}
