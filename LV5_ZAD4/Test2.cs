﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ZAD3
{
    class Test2
    {
        static void Main(string[] args)
        {
            Dataset dataset = new Dataset("Test.csv");

            IDataset virtualProxyLoggingDataset = new VirtualProxyLoggingDataset("Test.csv");
            IReadOnlyCollection<List<string>> data = virtualProxyLoggingDataset.GetData();

            DataConsolePrinter dataConsolePrinter = new DataConsolePrinter();
            dataConsolePrinter.ConsolePrinter(virtualProxyLoggingDataset);
        }
    }
}
